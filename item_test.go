package internal

import (
	"fmt"
	"testing"
)

var xkcdFirstItem = Item{1, 9, "https://xkcd.com/2082/", "https://xkcd.com/2082/", "Mercator Projection", "<img src=\"https://imgs.xkcd.com/comics/mercator_projection.png\" title=\"The other great lakes are just water on the far side of Canada Island. If you drive north from the Pacific northwest you actually cross directly into Alaska, although a few officials--confused by the Mercator distortion--have put up border signs.\" alt=\"The other great lakes are just water on the far side of Canada Island. If you drive north from the Pacific northwest you actually cross directly into Alaska, although a few officials--confused by the Mercator distortion--have put up border signs.\" />", "", map[string]string{}}

func setupSqliteItemService(t *testing.T, path string) sqliteItemService {
	conn, err := Connect(path)
	if err != nil {
		t.FailNow()
	}

	return NewItemService(conn).(sqliteItemService)
}

func compareItem(firstItem Item, secondItem Item) []error {
	var errors []error

	if firstItem.ID != secondItem.ID {
		errors = append(errors, fmt.Errorf("First item ID (%v) is not equal to the second item ID (%v)", firstItem.ID, secondItem.ID))
	}

	if firstItem.FeedID != secondItem.FeedID {
		errors = append(errors, fmt.Errorf("First item feed ID (%v) is not equal to the second item feed ID (%v)", firstItem.FeedID, secondItem.FeedID))
	}

	if firstItem.GUID != secondItem.GUID {
		errors = append(errors, fmt.Errorf("First item GUID (%v) is not equal to the second item GUID (%v)", firstItem.GUID, secondItem.GUID))
	}

	if firstItem.URL != secondItem.URL {
		errors = append(errors, fmt.Errorf("First item URL (%v) is not equal to the second item URL (%v)", firstItem.URL, secondItem.URL))
	}

	if firstItem.title != secondItem.title {
		errors = append(errors, fmt.Errorf("First item title (%v) is not equal to the second item title (%v)", firstItem.title, secondItem.title))
	}

	if firstItem.summary != secondItem.summary {
		errors = append(errors, fmt.Errorf("First item summary (%v) is not equal to the second item summary (%v)", firstItem.summary, secondItem.summary))
	}

	if firstItem.content != secondItem.content {
		errors = append(errors, fmt.Errorf("First item content (%v) is not equal to the second item content (%v)", firstItem.content, secondItem.content))
	}

	return errors
}

func TestItemTitle(t *testing.T) {
	tests := []struct {
		item          Item
		expectedTitle string
	}{
		{
			Item{1, 1, "GUID", "http://example.com", "original title", "original summary", "original content", map[string]string{}},
			"original title",
		}, {
			Item{1, 1, "GUID", "http://example.com", "original title", "original summary", "original content", map[string]string{"title": "field title"}},
			"field title",
		},
	}

	for _, tt := range tests {
		if tt.item.Title() != tt.expectedTitle {
			t.Errorf("Expected title to be %s, instead got %s", tt.expectedTitle, tt.item.Title())
		}
	}
}

func TestItemSummary(t *testing.T) {
	tests := []struct {
		item            Item
		expectedSummary string
	}{
		{
			Item{1, 1, "GUID", "http://example.com", "original title", "original summary", "original content", map[string]string{}},
			"original summary",
		}, {
			Item{1, 1, "GUID", "http://example.com", "original title", "original summary", "original content", map[string]string{"description": "field description"}},
			"field description",
		},
	}

	for _, tt := range tests {
		if tt.item.Summary() != tt.expectedSummary {
			t.Errorf("Expected summary to be %s, instead got %s", tt.expectedSummary, tt.item.Summary())
		}
	}
}

func TestItemContent(t *testing.T) {
	tests := []struct {
		item            Item
		expectedContent string
	}{
		{
			Item{1, 1, "GUID", "http://example.com", "original title", "original summary", "original content", map[string]string{}},
			"original content",
		}, {
			Item{1, 1, "GUID", "http://example.com", "original title", "original summary", "original content", map[string]string{"content": "field content"}},
			"field content",
		},
	}

	for _, tt := range tests {
		if tt.item.Content() != tt.expectedContent {
			t.Errorf("Expected content to be %s, instead got %s", tt.expectedContent, tt.item.Content())
		}
	}
}

func TestSqliteItemServiceFromID(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping sqliteItemService tests in short mode")
	}

	tests := []struct {
		itemID       ID
		expectedItem Item
	}{
		{ID(1), xkcdFirstItem},
	}

	itemService := setupSqliteItemService(t, "test.db")

	for _, tt := range tests {
		item, err := itemService.FromID(tt.itemID)
		if err != nil {
			t.Error(err)
		}

		errors := compareItem(item, tt.expectedItem)
		for _, err := range errors {
			t.Error(err)
		}
	}
}

func TestSqliteItemServiceFeedItemsRange(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping sqliteItemService tests in short mode")
	}

	tests := []struct {
		feedID     ID
		firstItem  Item
		itemsCount int
	}{
		{ID(9), xkcdFirstItem, 4},
	}

	itemService := setupSqliteItemService(t, "test.db")

	for _, tt := range tests {
		items, err := itemService.FeedItemsRange(tt.feedID, 0, 15)
		if err != nil {
			t.Error(err)
		}

		if len(items) != tt.itemsCount {
			t.Errorf("Expected to get %d items, got %d instead", tt.itemsCount, len(items))
		}

		if len(items) == 0 {
			// Can't compare firstItem if we didn't get any item
			continue
		}

		errors := compareItem(items[0], tt.firstItem)
		for _, err := range errors {
			t.Error(err)
		}
	}
}

func TestSqliteItemServiceCategoryItemsRange(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping sqliteItemService tests in short mode")
	}

	tests := []struct {
		categoryID ID
		firstItem  Item
		itemsCount int
	}{
		{ID(3), xkcdFirstItem, 5},
	}

	itemService := setupSqliteItemService(t, "test.db")

	for _, tt := range tests {
		items, err := itemService.CategoryItemsRange(tt.categoryID, 0, 5)
		if err != nil {
			t.Error(err)
		}

		if len(items) != tt.itemsCount {
			t.Errorf("Expected to get %d items, got %d instead", tt.itemsCount, len(items))
		}

		if len(items) == 0 {
			// Can't compare firstItem if we didn't get any item
			continue
		}

		errors := compareItem(items[0], tt.firstItem)
		for _, err := range errors {
			t.Error(err)
		}
	}
}
