CREATE TABLE users (
	username TEXT UNIQUE,
	password TEXT
);

INSERT INTO users (username, password) VALUES ("test", "$2a$10$McC5ZTDo.9K0yK8tvhgsCeoWk.hD9VXHwmMRMl/R5ilYgpMANI5Hm");

CREATE TABLE categories (
	user_id INTEGER REFERENCES users(rowid),
	name TEXT
);

INSERT INTO categories (user_id, name) VALUES (1, "Free Software"), (1, "Linux Desktop"), (1, "Comics");

CREATE TABLE feeds (
	url TEXT,
	name TEXT
);

INSERT INTO feeds (url, name) VALUES ("https://www.fsf.org/static/fsforg/rss/blogs.xml", "Free Software Foundation Blog"), ("https://planet.gnu.org/rss20.xml", "Planet GNU"), ("https://stallman.org/rss/rss.xml", "Richard Stallman's Political Notes"), ("https://fsfe.org/news/news.en.rss", "FSFE News");
INSERT INTO feeds (url, name) VALUES ("http://planet.gnome.org/atom.xml", "Planet GNOME"), ("http://planetkde.org/rss20.xml", "Planet KDE"), ("https://puri.sm/feed/", "Purism"), ("http://blog.system76.com/rss", "System76 Blog");
INSERT INTO feeds (url, name) VALUES ("https://xkcd.com/atom.xml", "XKCD"), ("http://feed.dilbert.com/dilbert/daily_strip?format=xml", "Dilbert");

CREATE TABLE feed_category_relations (
	feed_id INTEGER REFERENCES feeds(rowid),
	category_id INTEGER REFERENCES categories(rowid)
);

INSERT INTO feed_category_relations (feed_id, category_id) VALUES (1, 1), (2, 1), (3, 1), (4, 1);
INSERT INTO feed_category_relations (feed_id, category_id) VALUES (5, 2), (6, 2), (7, 2), (8, 2);
INSERT INTO feed_category_relations (feed_id, category_id) VALUES (9, 3), (10, 3);

CREATE TABLE items (
	feed_id INTEGER REFERENCES feeds(rowid),

	id TEXT UNIQUE,
	title TEXT,
	published INTEGER,
	summary TEXT,
	url TEXT,
	content TEXT
);

CREATE TABLE item_fields (
	item_id INTEGER REFERENCES items(rowid),
	key TEXT,
	value TEXT
);
