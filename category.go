package internal

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// Category contains a list of feeds and belog to a user
type Category struct {
	ID     ID     `db:"rowid"`
	UserID ID     `db:"user_id"`
	Name   string `db:"name"`
}

// CategoryService is a service that expose the necassary category operations
type CategoryService interface {
	FromID(catID ID) (Category, error)
	UserCategories(userID ID) ([]Category, error)

	UpdateCategory(c Category) error
}

type sqliteCategoryService struct {
	conn *sqlx.DB
}

// FromID return a category object with the given category ID
func (s sqliteCategoryService) FromID(catID ID) (Category, error) {
	var category Category

	row := s.conn.QueryRowx("SELECT rowid, user_id, name FROM categories WHERE rowid = $1", catID)
	err := row.StructScan(&category)
	if err != nil {
		return category, err
	}

	return category, nil
}

// UserCategories return all the categories of the user
func (s sqliteCategoryService) UserCategories(userID ID) ([]Category, error) {
	var categories []Category

	query := "SELECT rowid, user_id, name FROM categories WHERE user_id = $1"
	err := s.conn.Select(&categories, query, userID)
	if err != nil {
		return categories, err
	}

	return categories, err
}

func (s sqliteCategoryService) UpdateCategory(c Category) error {
	_, err := s.conn.Exec("UPDATE categories SET name = ?, user_id = ? WHERE rowid = ?", c.Name, c.UserID, c.ID)
	if err != nil {
		return fmt.Errorf("failed to update the category in the database: %v", err)
	}

	return nil
}

// NewCategoryService create a category service for the given database connection
func NewCategoryService(conn *sqlx.DB) CategoryService {
	return sqliteCategoryService{conn: conn}
}
