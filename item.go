package internal

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

const itemSQLColumns = "rowid, feed_id, id, url, title, summary, content"

// Item is a single entry in a feed
type Item struct {
	ID     ID `db:"rowid"`
	FeedID ID `db:"feed_id"`

	GUID string `db:"id"`
	URL  string `db:"url"`

	title   string
	summary string
	content string
	fields  map[string]string
}

// Title returns the real title of the item
func (i Item) Title() string {
	if val, ok := i.fields["title"]; ok {
		return val
	}

	return i.title
}

// Summary returns a short description about the item
func (i Item) Summary() string {
	if val, ok := i.fields["summary"]; ok {
		return val
	}

	if val, ok := i.fields["description"]; ok {
		return val
	}

	return i.summary
}

// Content returns the real content of the item
func (i Item) Content() string {
	if val, ok := i.fields["content"]; ok {
		return val
	}

	return i.content
}

// ItemService is a service that expose the necassary item operations
type ItemService interface {
	FromID(itemID ID) (Item, error)
	FeedItemsRange(feedID ID, start, end int) ([]Item, error)
	CategoryItemsRange(categoryID ID, start, end int) ([]Item, error)
}

type sqliteItemService struct {
	conn *sqlx.DB
}

// FromID returns the item with the given ID
func (s sqliteItemService) FromID(itemID ID) (Item, error) {
	var item Item
	query := fmt.Sprintf("SELECT %s FROM items WHERE rowid = $1", itemSQLColumns)
	err := s.conn.QueryRowx(query, itemID).Scan(&item.ID, &item.FeedID, &item.GUID, &item.URL, &item.title, &item.summary, &item.content)
	return item, err
}

// FeedItemsRange return all the items in a feed within a range limit
func (s sqliteItemService) FeedItemsRange(feedID ID, start, end int) ([]Item, error) {
	var items []Item
	query := fmt.Sprintf("SELECT %s FROM items WHERE feed_id = $1 LIMIT $2,$3", itemSQLColumns)
	rows, err := s.conn.Queryx(query, feedID, start, end)
	if err != nil {
		return items, fmt.Errorf("failed to get feed items range: %v", err)
	}

	for rows.Next() {
		var item Item
		err = rows.Scan(&item.ID, &item.FeedID, &item.GUID, &item.URL, &item.title, &item.summary, &item.content)
		if err != nil {
			return items, fmt.Errorf("failed to scan a row in feed items range: %v", err)
		}
		items = append(items, item)
	}

	return items, err
}

// CategoryItemsRange return all the items in a category within a range limit
func (s sqliteItemService) CategoryItemsRange(categoryID ID, start, end int) ([]Item, error) {
	var items []Item
	query := fmt.Sprintf("SELECT %s FROM items WHERE feed_id IN (SELECT feed_id FROM feed_category_relations WHERE category_id = $1) LIMIT $2,$3", itemSQLColumns)
	rows, err := s.conn.Queryx(query, categoryID, start, end)
	if err != nil {
		return items, fmt.Errorf("failed to get category items range: %v", err)
	}

	for rows.Next() {
		var item Item
		err = rows.Scan(&item.ID, &item.FeedID, &item.GUID, &item.URL, &item.title, &item.summary, &item.content)
		if err != nil {
			return items, fmt.Errorf("failed to scan a row in category items range: %v", err)
		}
		items = append(items, item)
	}

	return items, err
}

// NewItemService create an object which hold all the item operations on the database
func NewItemService(conn *sqlx.DB) ItemService {
	return sqliteItemService{conn: conn}
}
