package internal

import (
	"github.com/jmoiron/sqlx"
)

// Feed is a list of entries from a website with a connection to a user category
type Feed struct {
	ID         ID `db:"rowid"`
	FeedID     ID `db:"feed_id"`
	CategoryID ID `db:"category_id"`

	Name string `db:"name"`
	URL  string `db:"url"`
}

// FeedService is a service that expose the necassary feed operations
type FeedService interface {
	FromID(relationID ID) (Feed, error)
	CategoryFeeds(categoryID ID) ([]Feed, error)
}

type sqliteFeedService struct {
	conn *sqlx.DB
}

// FromID return a feed that have a relation with the given relationID
func (s sqliteFeedService) FromID(relationID ID) (Feed, error) {
	var feed Feed
	query := "SELECT relations.rowid, feed_id, category_id, feeds.name as name, feeds.url as url FROM feed_category_relations as relations INNER JOIN feeds ON relations.feed_id = feeds.rowid WHERE relations.rowid = $1"
	err := s.conn.QueryRowx(query, relationID).StructScan(&feed)

	return feed, err
}

// CategoryFeeds return all the feeds of the category
func (s sqliteFeedService) CategoryFeeds(categoryID ID) ([]Feed, error) {
	var feeds []Feed
	query := "SELECT relations.rowid, feed_id, category_id, feeds.name as name, feeds.url as url FROM feed_category_relations as relations INNER JOIN feeds ON relations.feed_id = feeds.rowid WHERE relations.category_id = $1"
	err := s.conn.Select(&feeds, query, categoryID)

	return feeds, err
}

// NewFeedService create a feed service for the given database connection
func NewFeedService(conn *sqlx.DB) FeedService {
	return sqliteFeedService{conn: conn}
}
