package internal

import (
	"github.com/jmoiron/sqlx"
)

// ID is the type of the database rows IDs
type ID int64

// Connect to the database
func Connect(path string) (*sqlx.DB, error) {
	return sqlx.Connect("sqlite3", path)
}
