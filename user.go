package internal

import (
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"

	_ "github.com/mattn/go-sqlite3" // the sqlite driver
)

// User is an object representing a user
type User struct {
	ID       ID     `db:"rowid"`
	Username string `db:"username"`
}

// UserService is a service that expose all the necassary user operations
type UserService interface {
	ValidateAuth(username, password string) (ID, error)
	ValidateToken(token string) (ID, error)
	FromID(userID ID) (User, error)
}

type sqliteUserService struct {
	conn *sqlx.DB
}

// ValidateAuth check that the username and password match the data stored in the database
func (s sqliteUserService) ValidateAuth(username, password string) (ID, error) {
	var userID ID
	var realPassword string
	row := s.conn.QueryRowx("SELECT rowid, password FROM users WHERE username = $1", username)

	err := row.Scan(&userID, &realPassword)
	if err != nil {
		return 0, fmt.Errorf("Failed to query the database: %v", err)
	}

	if err := bcrypt.CompareHashAndPassword([]byte(realPassword), []byte(password)); err != nil {
		return 0, fmt.Errorf("The provided password is incorrect: %v", err)
	}

	return userID, nil
}

// ValidateToken check that the token is correct and return the matching user ID
func (s sqliteUserService) ValidateToken(token string) (ID, error) {
	return 0, errors.New("Not Implemented")
}

// FromID create a User object from his ID
func (s sqliteUserService) FromID(userID ID) (User, error) {
	var user User

	row := s.conn.QueryRowx("SELECT rowid, username FROM users WHERE rowid = $1", userID)
	err := row.StructScan(&user)
	if err != nil {
		return user, fmt.Errorf("failed to run sql query: %v", err)
	}

	return user, nil
}

// NewUserService create an object which hold all the user operations on the database
func NewUserService(conn *sqlx.DB) UserService {
	return sqliteUserService{conn: conn}
}
