# Internal API

This package is a Go API for the database with services for each type of object.
It should be used by each component of the server so the server won't depend
on a single database implementation.