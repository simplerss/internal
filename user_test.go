package internal

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSqliteValidateAuth(t *testing.T) {
	tests := map[string]struct {
		username string
		password string
		result   ID
		err      bool
	}{
		"Correct":                         {username: "test", password: "12345", result: 1, err: false},
		"Incorrect Username":              {username: "user", password: "12345", result: 0, err: true},
		"Incorrect Password":              {username: "test", password: "password", result: 0, err: true},
		"Incorrect Username And Password": {username: "user", password: "password", result: 0, err: true},
		"SQL Injection":                   {username: "user OR 1 = 1", password: "12345", result: 0, err: true},
	}

	conn, err := Connect("./tests_resources/users/validate_auth.db")
	if err != nil {
		t.Fatalf("Failed to connect to the tests database: %v", err)
	}

	userService := NewUserService(conn)
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			id, err := userService.ValidateAuth(tc.username, tc.password)
			// if got an unexpected error or didn't get an expected error
			if !tc.err && err != nil {
				t.Fatalf("Unexpected error value: %v", err)
			}

			if id != tc.result {
				t.Fatalf("Expected: %v got: %v", tc.result, id)
			}
		})
	}
}

func TestSqliteUserServiceFromID(t *testing.T) {
	tests := map[string]struct {
		input ID
		want  User
		err   bool
	}{
		"Simple":            {input: 1, want: User{1, "test"}, err: false},
		"Non Existing User": {input: 0, want: User{}, err: true},
	}

	conn, err := Connect("./tests_resources/users/from_id.db")
	if err != nil {
		t.Fatalf("Failed to connect to the tests database: %v", err)
	}

	userService := NewUserService(conn)
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			user, err := userService.FromID(tc.input)
			// if got an unexpected error or didn't get an expected error
			if !tc.err && err != nil {
				t.Fatalf("Unexpected error value: %v", err)
			}

			diff := cmp.Diff(user, tc.want)
			if diff != "" {
				t.Fatalf(diff)
			}
		})
	}
}
