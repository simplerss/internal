module gitlab.com/simplerss/internal

require (
	github.com/google/go-cmp v0.3.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.9.0
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	google.golang.org/appengine v1.5.0 // indirect
)
